# Estructura de datos

Actividades:

* Presentación de transparencias del tema "Estructuras de datos"

Ejercicios:

**Ejercicio a entregar:** Lista de la compra

  * **Fecha de entrega:** 30 de octubre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/compra/
  * [Enunciado](compra/README.md).
