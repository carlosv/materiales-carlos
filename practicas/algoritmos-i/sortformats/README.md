### Ordenar formatos de imágenes por nivel de compresión

Escribe un programa que ordene sistemas de codificación de imagen en función del grado de compresión de los archivos, de menor a mayor compresión. Los formatos de archivo válidos se encuentran ordenados de menor a mayor compresión en la lista `formats`, que se proporciona en el fichero `plantilla.py` (ver más abajo).

El programa recibe una lista de nombres de formatos de compresión (que se usan habitualmente como extensiones de archivos con esos formatos de compresión) como argumentos de línea de comandos y las ordena según el grado de compresión. Los formatos de archivo se ordenan de menor a mayor compresión, y la lista resultante se muestra en la salida estándar.

El programa tendrá que usar el algoritmo de ordenación por selección que hemos visto en clase, y no podrá usar funciones de ordenación proporcionadas por Python (como `sort()`) o por módulos de terceros

Por ejemplo, una ejecución típica será como sigue:

```commandline
python3 sortformats.py jpg png gif eps webp

png jpg webp eps gif
```


El programa se llamará `sortformats.py`, y tendrá la estructura que se presenta en el fichero `plantilla.py` del repositorio plantilla:

* La función `sort_formats()` recibirá la lista de extensiones introducida por línea de comandos por el usuario y, devolverá la lista ordenada en función del grado de compresión de los archivos.
* La función `main()` recibirá la lista de extensiones introducida por línea de comandos por el usuario y, escribirá en pantalla la lista final de archivos ordenados.

Como verás en el fichero `plantilla.py`, en ese esquema, el código está estructurado en funciones y, la función principal para ordenar la lista (`sort_formats()`) será llamada desde la función `main()`. El "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main()`, que hará todo el trabajo. Para empezar a escribir tu programa, copia el contenido de `plantilla.py` en `sortformats.py`, y completes luego `sortformats.py` hasta que funcione como se especifica en este enunciado. El fichero `plantilla.py` no tiene que estar en el repositorio en el momento de la entrega de la práctica.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en EIF GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local, que será el proyecto de PyCharm donde realizarás el ejercicio.
* Copia el código de `plantilla.py` a  `sortformats.py`, y modifica su contenido hasta que el programa funcione como debe.
* Borra del repositorio el programa `plantilla.py` que no tendrá que estar en el repositorio de entrega.
* Crea al menos tres commits, durante este proceso, según vas haciendo cambios a tu programa.
* Sube los commits a tu repositorio en EIF GitLab.

Al terminar, comprueba en EIF GitLab que el fichero `sortformats.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_sortformats.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_sortformats.py
```

