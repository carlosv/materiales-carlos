import sys

def line(number: int):
    return str(number) * number

def triangle(number: int):
    if number > 9:
        raise ValueError('¡Tiene que ser menor o igual que 9!')
    triangulo = ""
    for i in range(1, number+1):
        triangulo = str(triangulo) + line(i) + '\n'

    return triangulo