num1 = int(input("Dame un número entero no negativo: "))

if num1 < 0:
    print("El número debe ser no negativo.")
else:
    num2 = int(input("Dame otro: "))
    
    if num2 < 0:
        print("El segundo número debe ser no negativo.")
    else:
        suma_impares = 0
        if num1 > num2:
            num1, num2 = num2, num1
        for num in range(num1, num2 + 1):
            if num % 2 != 0:  
                suma_impares += num
        print(f"La suma de los números impares entre {num1} y {num2} es: {suma_impares}")