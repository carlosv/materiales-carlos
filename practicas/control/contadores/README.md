### Contador de contadores

Escribe un programa que, dado un número, escriba en líneas sucesivas listas desde 1 hasta ese número, primero, y luego sucesivamente, hasta ese número menos uno, hasta ese número menos dos, y así hasta que sólo escribamos el número. Comprueba que el número que te han dado es entero (usando una excepción), y 1 o mayor que uno. Procura que el código sea lo más sencillo y legible posible, manteniendo la funcionalidad definida.

Un ejemplo de ejecución podría ser:

```shell
$ python3 contador_contadores.py
Dame un número entero mayor que 0: 5
1 2 3 4 5
1 2 3 4
1 2 3
1 2
1
```

Otro ejemplo:

```shell
$ python3 contador_contadores.py
Dame un número entero mayor que 0: 0
Dame un número entero mayor que 0: 2
1 2
1
```

Otro:

```shell
$ python3 contador_contadores.py
Dame un número entero mayor que 0: 3.4
Dame un número entero mayor que 0: 3
1 2 3
1 2
1
```
