### Número mauyor

Realiza un programa que pida un número, indique si es
mayor o menor que 10, y luego escriba desde ese
número hasta 1, decrementando de uno en uno. Por ejemplo:

```commandline
$ python3 numero_mayor.py
Dame un número entero: 11
Es mayor que 10
11
10
9
8
7
6
5
4
3
2
1
¡Hasta aquí hemos llegado!
```

Solución: [numero_mayor.py](numero_mayor.py)
